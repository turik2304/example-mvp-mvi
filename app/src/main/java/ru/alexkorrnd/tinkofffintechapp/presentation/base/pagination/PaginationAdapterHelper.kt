package ru.alexkorrnd.tinkofffintechapp.presentation.base.pagination

class PaginationAdapterHelper(
    private val onLoadMoreCallback: (offset: Int) -> Unit
) {

    private var isFullData = false

    fun onBind(adapterPosition: Int, totalItemCount: Int) {
        if (!isFullData && adapterPosition > totalItemCount - DEFAULT_LOAD_MORE_SUBSTITUTIONS) {
            onLoadMoreCallback(adapterPosition)
        }
    }

    companion object {
        private const val DEFAULT_LOAD_MORE_SUBSTITUTIONS = 2
    }
}
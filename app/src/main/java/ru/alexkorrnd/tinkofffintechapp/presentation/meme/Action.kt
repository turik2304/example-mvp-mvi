package ru.alexkorrnd.tinkofffintechapp.presentation.meme

import ru.alexkorrnd.tinkofffintechapp.data.api.Meme

sealed class Action {
    object LoadFirstPage : Action()

    object LoadNextPage : Action()

    object StopLoadingNextPage: Action()

    data class PageLoaded(val items: List<Meme>) : Action()

    data class ErrorLoadFirstPage(val error: Throwable) : Action()
}
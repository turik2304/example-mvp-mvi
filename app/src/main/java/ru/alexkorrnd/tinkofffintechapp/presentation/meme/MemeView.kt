package ru.alexkorrnd.tinkofffintechapp.presentation.meme

interface MemeView {

    fun render(state: State)

    fun handleUiEffect(uiEffect: UiEffect)
}

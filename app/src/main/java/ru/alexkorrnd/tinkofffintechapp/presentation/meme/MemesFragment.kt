package ru.alexkorrnd.tinkofffintechapp.presentation.meme

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_memes.*
import kotlinx.android.synthetic.main.inc_error_state.*
import ru.alexkorrnd.tinkofffintechapp.R
import ru.alexkorrnd.tinkofffintechapp.di.GlobalDI
import ru.alexkorrnd.tinkofffintechapp.presentation.base.SpacesItemDecoration
import ru.alexkorrnd.tinkofffintechapp.presentation.base.mvp.MvpFragment
import ru.alexkorrnd.tinkofffintechapp.presentation.base.pagination.PaginationAdapterHelper
import ru.alexkorrnd.tinkofffintechapp.presentation.meme.adapter.MemesAdapter
import ru.alexkorrnd.tinkofffintechapp.utils.userMessage

class MemesFragment : MvpFragment<MemeView, MemesPresenter>(), MemeView {

    private val adapter = MemesAdapter(
        PaginationAdapterHelper { _ -> getPresenter().input.accept(Action.LoadNextPage) }
    )

    override fun getPresenter(): MemesPresenter = GlobalDI.INSTANCE.presenter

    override fun getMvpView(): MemeView = this

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_memes, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = adapter
        recyclerView.addItemDecoration(SpacesItemDecoration(resources.getDimensionPixelOffset(R.dimen.item_space_size)))
        retryButton.setOnClickListener { getPresenter().input.accept(Action.LoadFirstPage) }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        getPresenter().input.accept(Action.LoadFirstPage)
    }

    override fun render(state: State) {
        loading.isVisible = state.isLoading

        recyclerView.isVisible = state.items.isNotEmpty()
        adapter.items = state.items

        emptyStateContainer.isVisible = state.isEmptyState

        errorStateContainer.isVisible = state.error != null
        state.error?.let { throwable ->  errorText.text = throwable.userMessage(requireContext()) }
    }

    override fun handleUiEffect(uiEffect: UiEffect) {
        when(uiEffect) {
            is UiEffect.NextPageLoadError -> {
                Snackbar.make(view!!, uiEffect.error.userMessage(requireContext()), Snackbar.LENGTH_SHORT)
                    .show()
            }
        }
    }

    companion object {
        const val TAG = "MemesFragment"

        fun newInstance() = MemesFragment()
    }
}
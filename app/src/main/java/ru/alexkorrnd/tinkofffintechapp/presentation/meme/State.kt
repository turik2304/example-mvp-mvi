package ru.alexkorrnd.tinkofffintechapp.presentation.meme

import ru.alexkorrnd.tinkofffintechapp.presentation.meme.adapter.NextPageLoader

data class State(
    val items: List<Any> = emptyList(),
    val isEmptyState: Boolean = false,
    val error: Throwable? = null,
    val isLoading: Boolean = false,
    val pageNumber: Int = INITIAL_PAGE
) {
    companion object {
        internal const val INITIAL_PAGE = 0
    }
}

internal fun State.reduce(action: Action): State {
    return when (action) {
        is Action.LoadFirstPage -> copy(
            isLoading = true,
            isEmptyState = false,
            error = null,
            pageNumber = State.INITIAL_PAGE
        )
        is Action.PageLoaded -> {
            val itemsList = items.filter { it !is NextPageLoader } + action.items
            copy(
                items = itemsList,
                isEmptyState = itemsList.isEmpty(),
                isLoading = false,
                pageNumber = pageNumber + 1
            )
        }
        is Action.ErrorLoadFirstPage -> copy(
            error = action.error,
            isEmptyState = false,
            isLoading = false
        )
        is Action.LoadNextPage -> copy(
            isEmptyState = false,
            isLoading = false,
            items = items.filter { it !is NextPageLoader } + listOf(NextPageLoader)
        )
        is Action.StopLoadingNextPage -> copy(
            items = items.filter { it !is NextPageLoader },
            isEmptyState = false
        )
    }
}

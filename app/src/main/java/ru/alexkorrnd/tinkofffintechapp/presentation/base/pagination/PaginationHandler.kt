package ru.alexkorrnd.tinkofffintechapp.presentation.base.pagination

import ru.alexkorrnd.tinkofffintechapp.presentation.base.ResourceProvider
import ru.alexkorrnd.tinkofffintechapp.utils.userMessage

class PaginationHandler(
    val paginator: Paginator,
    private val resourceProvider: ResourceProvider
) {

    fun process(error: Throwable, viewCallback: (message: String, isCritical: Boolean) -> Unit) {
        viewCallback(error.userMessage(resourceProvider), paginator.isFirstPage)
    }

}
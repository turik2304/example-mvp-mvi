package ru.alexkorrnd.tinkofffintechapp.data.api

class BaseResult<T>(
    val result: T
)
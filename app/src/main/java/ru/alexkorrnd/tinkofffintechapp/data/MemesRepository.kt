package ru.alexkorrnd.tinkofffintechapp.data

import io.reactivex.Single
import ru.alexkorrnd.tinkofffintechapp.data.api.Meme

class MemesRepository(
    private val service: DevelopersLifeService
) {

    fun loadMemes(pageNumber: Int): Single<List<Meme>> {
        return service.loadMemes(pageNumber)
            .map { it.result }
    }
}